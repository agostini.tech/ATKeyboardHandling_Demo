//
//  ATKeyboardHandling.swift
//  ATKeyboardHandling_Demo
//
//  Created by Dejan on 25/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit

class ATKeyboardHandling: NSObject
{
    @IBOutlet public weak var bottomConstraint: NSLayoutConstraint?
    @IBOutlet public weak var scrollView: UIScrollView?
    @IBInspectable public var padding: CGFloat = 20.0
    
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private var originalConstraintConstant: CGFloat?
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let animationDuration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue {
            
            guard self.originalConstraintConstant == nil else {
                return
            }
            
            self.originalConstraintConstant = self.bottomConstraint?.constant
            self.bottomConstraint?.constant -= keyboardSize.height
            
            UIView.animate(withDuration: animationDuration, animations: {
                self.scrollView?.layoutIfNeeded()
            }) { (_) in
                guard let firstResponder = UIResponder.currentFirstResponder as? UIView else {
                    return
                }
                
                var newFrame = firstResponder.frame
                newFrame.origin.y += self.padding
                self.scrollView?.scrollRectToVisible(newFrame, animated: true)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let constant = self.originalConstraintConstant {
            self.bottomConstraint?.constant = constant
        }
        self.originalConstraintConstant = nil
        
        if let animationDuration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue {
            UIView.animate(withDuration: animationDuration) {
                self.scrollView?.layoutIfNeeded()
            }
        }
    }
}

extension UIResponder {
    
    private static weak var _currentFirstResponder: UIResponder?
    
    static var currentFirstResponder: UIResponder? {
        _currentFirstResponder = nil
        UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder(_:)), to: nil, from: nil, for: nil)
        
        return _currentFirstResponder
    }
    
    @objc func findFirstResponder(_ sender: Any) {
        UIResponder._currentFirstResponder = self
    }
}
