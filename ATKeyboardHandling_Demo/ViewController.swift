//
//  ViewController.swift
//  ATKeyboardHandling_Demo
//
//  Created by Dejan on 25/06/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
